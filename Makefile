default: debug

ANDROID_HOME := $(shell which adb | sed 's,/platform-tools/adb,,')
ANDROID_SERIAL ?= $(shell adb devices | grep 'device$$' | cut -f1 | tr '\n' , | sed s/,$$//)

generate:
	make -j4 -C libmupdf generate
debug: generate
	ANDROID_HOME=$(ANDROID_HOME) ./gradlew assembleDebug
release: generate
	ANDROID_HOME=$(ANDROID_HOME) ./gradlew assembleRelease
install: generate
	ANDROID_HOME=$(ANDROID_HOME) ./gradlew installDebug
lint:
	ANDROID_HOME=$(ANDROID_HOME) ./gradlew lint
clean:
	rm -rf .gradle .externalNativeBuild build

run: install
	IFS=","; DEVICES="$(ANDROID_SERIAL)"; \
	for device in $$DEVICES; do \
		adb -s $$device shell input keyevent KEYCODE_WAKEUP; \
		adb -s $$device shell am start -n com.artifex.mupdfdemo/.ChoosePDFActivity; \
	done

distclean:
	make -C libmupdf nuke
	rm -rf .externalNativeBuild .gradle build
	rm -rf .idea/ local.properties mupdf-android-viewer-old.iml
